<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * Job Controller
 *
 * @property \App\Model\Table\JobTable $Job
 */
class JobController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $job = $this->paginate($this->Job);

        $this->set(compact('job'));
        $this->set('_serialize', ['job']);
    }

    /**
     * View method
     *
     * @param string|null $id Job id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $job = $this->Job->get($id, [
            'contain' => []
        ]);

        $this->set('job', $job);
        $this->set('_serialize', ['job']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $job = $this->Job->newEntity();
        if ($this->request->is('post')) {
            $job = $this->Job->patchEntity($job, $this->request->data);
            if ($this->Job->save($job)) {
                  $HolId = $this->request->data('id'); // Angelegte Id Holen
                  $GetEmail = $this->request->data('Email'); // Angelegte Email Holen
                  // Email Einstellungen
                  $email = new Email('default');
                  $email
                  ->from('Campus@schramm-websolutions.com')
                  ->to($GetEmail)
                  ->subject('Job angelegt mit der Id '.$HolId)
                  ->send('Danke für die Benutzung unserer Job Börse'."\n".'Sie können Ihren Job unter https://schramm-websolutions.com/app/job/edit/'.$HolId.' bearbeiten oder auch löschen');
                  $this->Flash->success(__('Job erfolgreich angelegt'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The job could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('job'));
        $this->set('_serialize', ['job']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Job id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $job = $this->Job->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $job = $this->Job->patchEntity($job, $this->request->data);
            if ($this->Job->save($job)) {
                $this->Flash->success(__('Ihr Jobangebot wurde erfolgreich geändert.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The job could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('job'));
        $this->set('_serialize', ['job']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Job id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $job = $this->Job->get($id);
        if ($this->Job->delete($job)) {
            $this->Flash->success(__('Ihr Jobangebot wurde erfolgreich gelöscht.'));
        } else {
            $this->Flash->error(__('The job could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
