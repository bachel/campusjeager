<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Liste aller Jobs'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="job form large-9 medium-8 columns content">
    <?= $this->Form->create($job) ?>
    <fieldset>
        <legend><?= __('Jobangebot anlegen') ?></legend>
        <?php
            $customId = uniqid();
            $ErstellDatum = date("Y-m-d");
            echo $this->Form->input('id',['value' => $customId]);
            echo $this->Form->input('Anbieter');
            echo $this->Form->input('Kurzbeschreibung');
            echo $this->Form->input('Beschreibung');
            echo $this->Form->input('Email',['type' => 'email','required' => true]);
            echo $this->Form->hidden('Erstellt',['value' => $ErstellDatum]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Abschicken')) ?>
    <?= $this->Form->end() ?>
</div>
