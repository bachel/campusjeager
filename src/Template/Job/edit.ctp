<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Optionen') ?></li>
        <li><?= $this->Form->postLink(
                __('Job Löschen'),
                ['action' => 'delete', $job->id],
                ['confirm' => __('Sind Sie sicher das Sie Ihr Jobangebot mit der ID # {0} Löschen möchten ?', $job->id)]
            )
        ?></li>
    </ul>
</nav>
<div class="job form large-9 medium-8 columns content">
    <?= $this->Form->create($job) ?>
    <fieldset>
        <legend><?= __('Jobangebot bearbeiten') ?></legend>
        <?php
           $ErstellDatum = date("Y-m-d");
            echo $this->Form->input('id');
            echo $this->Form->input('Anbieter');
            echo $this->Form->input('Kurzbeschreibung');
            echo $this->Form->input('Beschreibung');
            echo $this->Form->hidden('Erstellt',['value' => $ErstellDatum]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Ändern')) ?>
    <?= $this->Form->end() ?>
</div>
