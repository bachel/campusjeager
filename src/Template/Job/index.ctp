<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Auswahl') ?></li>
        <li><?= $this->Html->link(__('Jobangebot erstellen'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="job index large-9 medium-8 columns content">
    <h3><?= __('Liste aller Jobs') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('Arbeitgeber') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Stellenangebot') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Veröffentlich') ?></th>
                <th scope="col" class="actions"><?= __('Aktionen') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($job as $job): ?>
            <tr>
                <td><?= h($job->Anbieter) ?></td>
                <td><?= h($job->Kurzbeschreibung) ?></td>
                <td><?= h($job->Erstellt) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Mehr Anzeigen'), ['action' => 'view', $job->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
