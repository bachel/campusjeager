<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Auswahl') ?></li>
        <li><?= $this->Html->link(__('Alle Jobs Anzeigen'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Jobangebot erstellen'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="job view large-9 medium-8 columns content">
    <h3> Job Angebot von <?= h($job->Anbieter) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Firma') ?></th>
            <td><?= h($job->Anbieter) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Veröffentlich') ?></th>
            <td><?= h($job->Erstellt) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Stellenbeschreibung') ?></h4>
        <?= $this->Text->autoParagraph(h($job->Beschreibung)); ?>
    </div>
</div>
